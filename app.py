from flask import Flask, render_template
import redis
import psycopg2
from datetime import datetime

app = Flask(__name__)

# Konfigurace připojení k databázi PostgreSQL
postgres_conn_str = "dbname='yourdb' user='postgres' host='localhost' password='yourpassword'"
# Konfigurace připojení k Redis
redis_conn_str = {'host': 'localhost', 'port': 6379, 'db': 0, 'decode_responses': True}

# Připojení k Redis
redis_client = redis.StrictRedis(**redis_conn_str)

# Funkce pro připojení k PostgreSQL a provedení SQL dotazu
def execute_sql(sql):
    conn = psycopg2.connect(postgres_conn_str)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    conn.close()

# Hlavní cesta aplikace
@app.route('/')
def index():
    # Uložení aktuálního data a času do PostgreSQL
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    execute_sql("INSERT INTO visits (visit_time) VALUES ('%s')" % current_time)

    # Zvýšení counteru v Redis
    page_views = redis_client.incr("page_views")

    # Zobrazení HTML stránky
    return render_template('index.html', current_time=current_time, page_views=page_views)

if __name__ == '__main__':
    app.run(debug=True)
